# sensedia-api-error-monitor
Jenknis setup for monitoring errors in the API Gateway.

# Requirements
The script is written in NodeJS and it makes use of external libraries `request` and `argparse`.

# Running the script
Run the script with the following command `node sensedia-api-error-monitor.js -h` to get help and instructions of usage.

The script accepts the following parameters:
- `--auth` is the SensediaAuth token, used for accessing API metrics
- `--url` address of Sensedia's API Manager
- `--environment` filter for environment
- `--window` time window for monitoring errors
- `--client_error` accepted pecentage of client errors
- `--server_error` accepted pecentage of server errors
- `--minimum_calls` minimum number of calls for error evaluation (default 0)

When defined, the configuration file will discard the other configuration. Therefore, the use of configuration file and arguments are excluisive.

# Docker container
The repository commit spins a Docker image build and the image can be used as a binary using the following command:
- `docker run anishitani/sensedia-api-error-monitor -h`

The same arguments from the scripted approach are applied to the containerized version.

Note: The script creates a file with a formatted e-mail body. This file is created as `/ws/output/arquivo.txt`. So the volume should be mounted to recevei this file.

- `docker run -v $PWD/output:/ws/output sensedia/sensedia-api-error-monitor --auth AUTH --url MANAGER --environment ENVIRONMENT --window WINDOW --client_error CLIENT_ERROR --server_error SERVER_ERROR`